#
# Yangfl <mmyangfl@gmail.com>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: sections\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-12-11 15:50+0800\n"
"PO-Revision-Date: 2018-01-21 01:08+0800\n"
"Last-Translator: Boyuan Yang <073plan@gmail.com>\n"
"Language-Team: <debian-l10n-chinese@lists.debian.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 2.0.5\n"

#: lib/Packages/Sections.pm:12
msgid "Administration Utilities"
msgstr "实用管理工具"

#: lib/Packages/Sections.pm:13
msgid "Utilities to administer system resources, manage user accounts, etc."
msgstr "管理系统资源，维护用户账号等实用工具。"

#: lib/Packages/Sections.pm:14
msgid "Base Utilities"
msgstr "基础实用工具"

#: lib/Packages/Sections.pm:15
msgid "Basic needed utilities of every Debian system."
msgstr "任何 Debian 系统必备的实用工具。"

#: lib/Packages/Sections.pm:16
msgid "Mono/CLI"
msgstr "Mono/CLI"

#: lib/Packages/Sections.pm:17
msgid "Everything about Mono and the Common Language Infrastructure."
msgstr "一切有关 Mono 和 Common Language Infrastructure 的内容。"

#: lib/Packages/Sections.pm:18
msgid "Communication Programs"
msgstr "通讯程序"

#: lib/Packages/Sections.pm:19
msgid "Software to use your modem in the old fashioned style."
msgstr "用于您老式调制解调器的软件。"

#: lib/Packages/Sections.pm:20
msgid "Databases"
msgstr "数据库"

#: lib/Packages/Sections.pm:21
msgid "Database Servers and Clients."
msgstr "数据库服务器和客户端。"

#: lib/Packages/Sections.pm:22
msgid "Debug packages"
msgstr "调试包"

#: lib/Packages/Sections.pm:23
msgid ""
"Packages providing debugging information for executables and shared "
"libraries."
msgstr "提供可执行文件和共享库调试信息的软件包。"

#: lib/Packages/Sections.pm:24
msgid "Development"
msgstr "开发"

#: lib/Packages/Sections.pm:25
msgid ""
"Development utilities, compilers, development environments, libraries, etc."
msgstr "实用开发工具，编译器，开发环境，链接库，等等。"

#: lib/Packages/Sections.pm:26
msgid "Documentation"
msgstr "文档"

#: lib/Packages/Sections.pm:27
msgid ""
"FAQs, HOWTOs and other documents trying to explain everything related to "
"Debian, and software needed to browse documentation (man, info, etc)."
msgstr ""
"FAQ，HOWTO，以及其他的、尝试讲解有关 Debian 一切的文档，以及阅读文档时所需的"
"软件(man，info，等)。"

#: lib/Packages/Sections.pm:28
msgid "Editors"
msgstr "编辑器"

#: lib/Packages/Sections.pm:29
msgid "Software to edit files. Programming environments."
msgstr "编辑文件的软件。程序编写环境。"

#: lib/Packages/Sections.pm:30
msgid "Education"
msgstr "教育"

#: lib/Packages/Sections.pm:31
msgid "Software for learning and teaching."
msgstr "学习和教育用软件。"

#: lib/Packages/Sections.pm:32
msgid "Electronics"
msgstr "电子工程学"

#: lib/Packages/Sections.pm:33
msgid "Electronics utilities."
msgstr "实用电子工程学工具。"

#: lib/Packages/Sections.pm:34
msgid "Embedded software"
msgstr "嵌入式软件"

#: lib/Packages/Sections.pm:35
msgid "Software suitable for use in embedded applications."
msgstr "适合于嵌入式应用的软件。"

#: lib/Packages/Sections.pm:36
msgid "Fonts"
msgstr "字体"

#: lib/Packages/Sections.pm:37
msgid "Font packages."
msgstr "字体软件包。"

#: lib/Packages/Sections.pm:38
msgid "Games"
msgstr "游戏"

#: lib/Packages/Sections.pm:39
msgid "Programs to spend a nice time with after all this setting up."
msgstr "用来消遣娱乐的程序。"

#: lib/Packages/Sections.pm:40
msgid "GNOME"
msgstr "GNOME"

#: lib/Packages/Sections.pm:41
msgid ""
"The GNOME desktop environment, a powerful, easy to use set of integrated "
"applications."
msgstr "GNOME 桌面环境，一组强大、易用、集成的应用程序。"

#: lib/Packages/Sections.pm:42
msgid "GNU R"
msgstr "GNU R"

#: lib/Packages/Sections.pm:43
msgid "Everything about GNU R, a statistical computation and graphics system."
msgstr "一切有关 GNU R 的内容，一个统计计算和图形系统。"

#: lib/Packages/Sections.pm:44
msgid "GNUstep"
msgstr "GNUstep"

#: lib/Packages/Sections.pm:45
msgid "The GNUstep environment."
msgstr "GNUstep 环境。"

#: lib/Packages/Sections.pm:46
msgid "Graphics"
msgstr "图形"

#: lib/Packages/Sections.pm:47
msgid "Editors, viewers, converters... Everything to become an artist."
msgstr "编辑器，浏览器，转换器……成为一名艺术家所需的一切。"

#: lib/Packages/Sections.pm:48
msgid "Ham Radio"
msgstr "业余无线电"

#: lib/Packages/Sections.pm:49
msgid "Software for ham radio."
msgstr "业余无线电软件。"

#: lib/Packages/Sections.pm:50
msgid "Haskell"
msgstr "Haskell"

#: lib/Packages/Sections.pm:51
msgid "Everything about Haskell."
msgstr "一切有关 Haskell 的内容。"

#: lib/Packages/Sections.pm:52
msgid "Web Servers"
msgstr "网页服务器"

#: lib/Packages/Sections.pm:53
msgid "Web servers and their modules."
msgstr "网页服务器及其模块。"

#: lib/Packages/Sections.pm:54
msgid "Interpreters"
msgstr "解释器"

#: lib/Packages/Sections.pm:55
msgid "All kind of interpreters for interpreted languages. Macro processors."
msgstr "各种解释型语言的解释器。宏处理器。"

#: lib/Packages/Sections.pm:56
msgid "Introspection"
msgstr "Introspection"

#: lib/Packages/Sections.pm:57
msgid "Machine readable introspection data for use by development tools."
msgstr "供开发工具使用的机器可读内省信息"

#: lib/Packages/Sections.pm:58
msgid "Java"
msgstr "Java"

#: lib/Packages/Sections.pm:59
msgid "Everything about Java."
msgstr "一切有关 Java 的内容。"

#: lib/Packages/Sections.pm:60
msgid "JavaScript"
msgstr "JavaScript"

#: lib/Packages/Sections.pm:61
msgid "JavaScript programming language, libraries, and development tools."
msgstr "JavaScript 编程语言、库和开发工具。"

#: lib/Packages/Sections.pm:62
msgid "KDE"
msgstr "KDE"

#: lib/Packages/Sections.pm:63
msgid ""
"The K Desktop Environment, a powerful, easy to use set of integrated "
"applications."
msgstr "KDE 桌面环境。一组强大的、易用的、集成的应用程序。"

#: lib/Packages/Sections.pm:64
msgid "Kernels"
msgstr "内核"

#: lib/Packages/Sections.pm:65
msgid "Operating System Kernels and related modules."
msgstr "操作系统内核和相关模块。"

#: lib/Packages/Sections.pm:66
msgid "Library development"
msgstr "链接库开发"

#: lib/Packages/Sections.pm:67
msgid "Libraries necessary for developers to write programs that use them."
msgstr "开发者要编写一些调用链接库的程序时所必需的链接库。"

#: lib/Packages/Sections.pm:68
msgid "Libraries"
msgstr "链接库"

#: lib/Packages/Sections.pm:69
msgid ""
"Libraries to make other programs work. They provide special features to "
"developers."
msgstr "提供其他程序工作的链接库。是它们让开发者富有特色。"

#: lib/Packages/Sections.pm:70
msgid "Lisp"
msgstr "Lisp"

#: lib/Packages/Sections.pm:71
msgid "Everything about Lisp."
msgstr "一切有关 Lisp 的内容。"

#: lib/Packages/Sections.pm:72
msgid "Language packs"
msgstr "语言包"

#: lib/Packages/Sections.pm:73
msgid "Localization support for big software packages."
msgstr "大型软件包的本地化支持。"

#: lib/Packages/Sections.pm:74
msgid "Mail"
msgstr "邮件"

#: lib/Packages/Sections.pm:75
msgid "Programs to route, read, and compose E-mail messages."
msgstr "投递、阅读、撰写电子邮件的程序。"

#: lib/Packages/Sections.pm:76
msgid "Mathematics"
msgstr "数学"

#: lib/Packages/Sections.pm:77
msgid "Math software."
msgstr "数学软件。"

#: lib/Packages/Sections.pm:78
msgid "Meta packages"
msgstr "元包"

#: lib/Packages/Sections.pm:79
msgid "Packages that mainly provide dependencies on other packages."
msgstr "主要用于提供对其它软件包的依赖的软件包。"

#: lib/Packages/Sections.pm:80
msgid "Miscellaneous"
msgstr "五花八门"

#: lib/Packages/Sections.pm:81
msgid "Miscellaneous utilities that didn't fit well anywhere else."
msgstr "放在其他地方都不合适的各式各样的实用程序。"

#: lib/Packages/Sections.pm:82
msgid "Network"
msgstr "网络"

#: lib/Packages/Sections.pm:83
msgid "Daemons and clients to connect your system to the world."
msgstr "服务端程序和客户端程序，使您的系统与世界相连接。"

#: lib/Packages/Sections.pm:84
msgid "Newsgroups"
msgstr "新闻组"

#: lib/Packages/Sections.pm:85
msgid "Software to access Usenet, to set up news servers, etc."
msgstr "访问新闻网，设置新闻服务器等软件。"

#: lib/Packages/Sections.pm:86
msgid "OCaml"
msgstr "OCaml"

#: lib/Packages/Sections.pm:87
msgid "Everything about OCaml, an ML language implementation."
msgstr "一切有关 OCaml 的内容，一个 ML 语言的实现。"

#: lib/Packages/Sections.pm:88
msgid "Old Libraries"
msgstr "旧的链接库"

#: lib/Packages/Sections.pm:89
msgid ""
"Old versions of libraries, kept for backward compatibility with old "
"applications."
msgstr "老版本的链接库，为了向后兼容旧程序而继续保留。"

#: lib/Packages/Sections.pm:90
msgid "Other OS's and file systems"
msgstr "其他操作系统和文件系统"

#: lib/Packages/Sections.pm:91
msgid ""
"Software to run programs compiled for other operating systems, and to use "
"their filesystems."
msgstr "使得在其他操作系统上编译的软件得以运行，以及使用它们的文件系统。"

#: lib/Packages/Sections.pm:92
msgid "Perl"
msgstr "Perl"

#: lib/Packages/Sections.pm:93
msgid "Everything about Perl, an interpreted scripting language."
msgstr "一切有关 Perl 的内容，一种解释型的脚本语言。"

#: lib/Packages/Sections.pm:94
msgid "PHP"
msgstr "PHP"

#: lib/Packages/Sections.pm:95
msgid "Everything about PHP."
msgstr "一切有关 PHP 的内容。"

#: lib/Packages/Sections.pm:96
msgid "Python"
msgstr "Python"

#: lib/Packages/Sections.pm:97
msgid ""
"Everything about Python, an interpreted, interactive object oriented "
"language."
msgstr "一切有关 Python 的内容。Python 是一种解释型、面向对象的语言。"

#: lib/Packages/Sections.pm:98
msgid "Ruby"
msgstr "Ruby"

#: lib/Packages/Sections.pm:99
msgid "Everything about Ruby, an interpreted object oriented language."
msgstr "一切有关 Ruby 的内容。Ruby 是一种解释型、面向对象的语言。"

#: lib/Packages/Sections.pm:100
msgid "Rust"
msgstr "Rust"

#: lib/Packages/Sections.pm:101
msgid "Rust programming language, library crates, and development tools"
msgstr "Rust 编程语言、crates 库和相关开发工具"

#: lib/Packages/Sections.pm:102
msgid "Science"
msgstr "科研"

#: lib/Packages/Sections.pm:103
msgid "Basic tools for scientific work"
msgstr "科研工作的初级工具"

#: lib/Packages/Sections.pm:104
msgid "Shells"
msgstr "Shells"

#: lib/Packages/Sections.pm:105
msgid "Command shells. Friendly user interfaces for beginners."
msgstr "命令行。友好的用户和计算机的交互界面。"

#: lib/Packages/Sections.pm:106
msgid "Sound"
msgstr "声音"

#: lib/Packages/Sections.pm:107
msgid ""
"Utilities to deal with sound: mixers, players, recorders, CD players, etc."
msgstr "处理声音的实用程序: 混频器，播放器，录音器，CD 播放器，等等。"

#: lib/Packages/Sections.pm:108
msgid "Tasks"
msgstr "Tasks"

#: lib/Packages/Sections.pm:109
msgid ""
"Packages that are used by 'tasksel', a simple interface for users who want "
"to configure their system to perform a specific task."
msgstr ""
"由“tasksel”使用的软件包；tasksel 是一个简单的用户界面，可用来配置系统完成特"
"定的任务。"

#: lib/Packages/Sections.pm:110
msgid "TeX"
msgstr "TeX"

#: lib/Packages/Sections.pm:111
msgid "The famous typesetting software and related programs."
msgstr "流行的文档排版及相关处理程序。"

#: lib/Packages/Sections.pm:112
msgid "Text Processing"
msgstr "文本处理"

#: lib/Packages/Sections.pm:113
msgid "Utilities to format and print text documents."
msgstr "格式化和打印文本文档的实用工具。"

#: lib/Packages/Sections.pm:114
msgid "Translations"
msgstr "翻译"

#: lib/Packages/Sections.pm:115
msgid "Translation packages and language support meta packages."
msgstr "一些提供翻译支持或语言支持的，自身不含内容但却依赖其他软件包的元包。"

#: lib/Packages/Sections.pm:116
msgid "Utilities"
msgstr "实用工具"

#: lib/Packages/Sections.pm:117
msgid ""
"Utilities for file/disk manipulation, backup and archive tools, system "
"monitoring, input systems, etc."
msgstr "文件/磁盘操作，备份和打包工具，系统监控，输入系统，等等。"

#: lib/Packages/Sections.pm:118
msgid "Version Control Systems"
msgstr "版本控制系统"

#: lib/Packages/Sections.pm:119
msgid "Version control systems and related utilities."
msgstr "版本控制系统和相关实用程序。"

#: lib/Packages/Sections.pm:120
msgid "Video"
msgstr "视频"

#: lib/Packages/Sections.pm:121
msgid "Video viewers, editors, recording, streaming."
msgstr "查看、编辑、录制、推流视频。"

#: lib/Packages/Sections.pm:122
msgid "Virtual packages"
msgstr "虚包"

#: lib/Packages/Sections.pm:123
msgid "Virtual packages."
msgstr "虚拟的、逻辑的、概念上的软件包。"

#: lib/Packages/Sections.pm:124
msgid "Web Software"
msgstr "网页软件"

#: lib/Packages/Sections.pm:125
msgid "Web servers, browsers, proxies, download tools etc."
msgstr "网页服务器，浏览器，代理，下载工具等。"

#: lib/Packages/Sections.pm:126
msgid "X Window System software"
msgstr "X 窗口系统软件"

#: lib/Packages/Sections.pm:127
msgid ""
"X servers, libraries, fonts, window managers, terminal emulators and many "
"related applications."
msgstr "X 服务器，链接库，字体，窗口管理器，虚拟终端以及许多有关的应用程序。"

#: lib/Packages/Sections.pm:128
msgid "Xfce"
msgstr "Xfce"

#: lib/Packages/Sections.pm:129
msgid "Xfce, a fast and lightweight Desktop Environment."
msgstr "Xfce，一个快速轻量的桌面环境。"

#: lib/Packages/Sections.pm:130
msgid "Zope/Plone Framework"
msgstr "Zope/Plone 框架"

#: lib/Packages/Sections.pm:131
msgid "Zope Application Server and Plone Content Managment System."
msgstr "Zope 应用服务器和 Plone 内容管理系统。"

#: lib/Packages/Sections.pm:132
msgid "debian-installer udeb packages"
msgstr "Debian 安装程序 udeb 包"

#: lib/Packages/Sections.pm:133
msgid ""
"Special packages for building customized debian-installer variants. Do not "
"install them on a normal system!"
msgstr ""
"一组特殊的包用于创建可定制的 Debian 安装程序的变体。请不要在一个普通系统上安"
"装它们！"
